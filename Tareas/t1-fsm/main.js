const eventEmiter = require ('./event-emiter');
const Behavior = require ('./Behavior');

new Behavior("Behavior","rest"); //Se declara el estado inicial

eventEmiter.send("element in your area","Behavior"); //Estando en el estado descansando se le envia un mensaje para que cambie a molesto

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

eventEmiter.send("element in your area","Behavior"); //Estando en el estado descansando se le envia un mensaje para que cambie a molesto

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

eventEmiter.send("element outside your area","Behavior"); //Estando en el estado descansando se le envia un mensaje para que cambie a molesto

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

eventEmiter.send("injured","Behavior");

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

eventEmiter.send("injured","Behavior");

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

eventEmiter.send("update","Behavior");
eventEmiter.update(); //quitar

//eventEmiter.send("on");

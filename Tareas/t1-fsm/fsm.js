"use strict";

const eventEmiter = require('./event-emiter');
/**
 * Maquina de estados finita.
 */
module.exports = class Fsm 
{
  constructor(agent, states) 
  {
    this._agent = agent;
    this._states = states;
    this._currentState = undefined;
    eventEmiter.register(this);
  }
  
  id() {
    return this._agent.id();
  }
  
  agent() {
    return this._agent;
  } 
  /**
   * Ciclo:
   * 1. Si el mensaje es "update"
   *   - mensaje especial para hacer update de los estados
   * 2. Si el mensaje no es "update"
   *   - recorrer los estados y ver si alguna lo reconoce
   *   - si lo reconoce activar el estado
   */
  onMessage(eventEmitter, event) 
  {    
    if (event.msg === "update")
    {
      if (this._currentState) 
      {
        this._currentState.onUpdate(eventEmitter, this, event);
      }
    } 
    else
    {
      const state = this._states.find((state) => state.accepts(eventEmitter, this, event));

      if (state && (state !== this._currentState))
      {
        if (this._currentState) 
        {
          this._currentState.onExit(eventEmitter, this, event);
        }
        this._currentState = state;
        this._currentState.onEnter(eventEmitter, this, event);
      }
    }
  }  
}
"use strict";

const State = require ('./state');
const Fsm = require ('./fsm');
/**
 * Estado Descansado
 */
class Rest extends State 
{
  accepts(eventEmitter, fsm, event) 
  {
      if(fsm.agent().status() === "upset" && event.msg === "element outside your area")
        return true;
      else if(fsm.agent().status() === "upset" && event.msg === "heal")
      {
        eventEmitter.send("update", event.id);
        fsm.agent().setHealed(false);
        return true;
      }
      else
        return false;
  }
  
  onEnter(eventEmitter, fsm, event) 
  {
    fsm.agent()._healingCont = 3;     
    fsm.agent().rest();
  }
  
  onUpdate(eventEmitter, fsm, event) 
  {
    fsm.agent().printStatus();
  }
}

/**
 * Estado Molesto
 */
class Upset extends State 
{
  accepts(eventEmitter, fsm, event) 
  {
    return fsm.agent().status() === "rest" && event.msg === "element in your area";
  }
  
  onEnter(eventEmitter, fsm, event) 
  {
    fsm.agent()._healingCont = 3;
    fsm.agent().upset();
  }
  
  onUpdate(eventEmitter, fsm, event) 
  {
    if(fsm.agent().heal())
    {
      eventEmitter.send("heal", event.id);
    }
    else
      fsm.agent().printStatus();
  }
}

/**
 * Estado Enojado
 */

class Angry extends State 
{  
  accepts(eventEmitter, fsm, event) 
  {
    if(fsm.agent().status() === "rest" && event.msg === "injured")
      return true;
    else if(fsm.agent().status() === "upset" && event.msg === "injured")
      return true;
    else if(fsm.agent().status() === "furious" && event.msg === "heal")
    {
      eventEmitter.send("update", event.id);
      fsm.agent().setHealed(false);
      return true;
    }
    else
      return false;
  }
  
  onEnter(eventEmitter, fsm, event) {
    fsm.agent()._healingCont = 3;
    fsm.agent().angry();
  }
  
  onUpdate(eventEmitter, fsm, event) {
    fsm.agent().printStatus();
  }
}

/**
 * Estado Furioso
 */
class Furious extends State {
  accepts(eventEmitter, fsm, event) 
  {
    return fsm.agent().status() === "angry" && event.msg === "injured";
  }
  
  onEnter(eventEmitter, fsm, event) 
  {
    fsm.agent()._healingCont = 3;
    fsm.agent().furious();
  }
  
  onUpdate(eventEmitter, fsm, event) 
  {
    if(fsm.agent().heal())
    {
      eventEmitter.send("heal", event.id);
    }
    else
      fsm.agent().printStatus();
  }
}

/**
 * Estados
 */
const STATES = [new Rest(), new Upset(), new Angry(), new Furious()];

/**
 * Agente
 */
module.exports = class Behavior 
{ 
  constructor(id, status) 
  {
    this._id = id;
    this._status = status;
    this._fsm = new Fsm(this, STATES);
    this._healingCont = 3;
  }

  id() {
    return this._id;
  }

  status()
  {
    return this._status;
  }

  healed()
  {
    return this._healed;
  }

  setHealed(healed)
  {
    this._healed = healed;
  }

  heal()
  {
    if(this._healingCont <= 1)
    {
      console.log(`${this._id} - sanado`);
      this._healingCont = 3;
      return true;
    }
    else
    {
      this._healingCont--;
      console.log(`${this._id} - sanando...`);
      return false;
    }
  }

  rest() {
    this._status = "rest";
    console.log(`${this._id} - descansando...`);
  }

  upset() {
    this._status = "upset";
    console.log(`${this._id} - molestando...`);
  }

  angry() {
    this._status = "angry";
    console.log(`${this._id} - enojando...`);
  }

  furious() 
  {
    this._status = "furious";
    console.log(`${this._id} - enfureciendo...`);
  }

  printStatus()
  {
    if(this._status === "rest")
      console.log(`${this._id} - Descansado`);
    else if(this._status === "upset")
      console.log(`${this._id} - Molesto`);
    else if(this._status === "angry")
      console.log(`${this._id} - Enojado`);
    else
      console.log(`${this._id} - Furioso`);    
  }
}



"use strict";
/**
 * Contrato de implementacion para un estado
 */
module.exports = class State {
  constructor() {
  }

  /**
   * Si el estado reconoce el mensaje regresa verdadero.
   */
  accepts(event) {
    return false;
  }

  /**
   * Se llama cada vez que el estado se activa
   */
  onEnter(eventEmitter, fsm, event) {     
  
  }
  /**
   * Si el estado esta activo se llama con cada ciclo
   */
  onUpdate(eventEmitter, fsm, event) {     
  }
  
  /**
   * Se llama cada vez que el estado se desactiva
   */
  onExit(eventEmitter, fsm, event) {     
  }
}
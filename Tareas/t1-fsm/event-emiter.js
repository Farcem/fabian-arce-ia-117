"use strict";

module.exports = (() => {
  /**
   * Simgleton object
   * FSM subscribe to receive messages
   * Se encarga de recibir y despachar los eventos del sistema
   * Los mensajes generados dentro del ciclo se despachan hasta el siguiente ciclo
   */
  class EventEmitter {
    
    constructor() {
      this._FSM = [];
      this._FSMById = new Map();
      this._queue = [];
    }
    /**
     * Despachar los mensajes que esten en la cola
     */
    update() {
      for(;this._queue.length > 0;) {
        this._queue.shift()();
      }
    }
    /**
     * Registrar objetos con capcidad de recibir mensajes. fsm.onMessage(event)
     */
    register(fsm) {
      this._FSMById.set(fsm.id(), fsm);
      this._FSM.push(fsm);
    }
    /**
     * Enviar mensaje, 
     * Si el id es no se especifica se envia a todos los objetos 
     */
    send(msg, id) {
      this._send({
        id: id,
        msg: msg
      });
    }
    /**
     * processo interno para enviar el mensaje.
     */
    _send(event) {      
      if (event.id) {
        this._sendPrivateMessage(event);
      } else {
        this._sendToAll(event);
      }
    }
    /**
     * Enviar a un objeto especifico
     */
    _sendPrivateMessage(event) {
      if (this._FSMById.has(event.id)) {
        const self = this;
        this._addToQueue(() => {
          const fsm = self._FSMById.get(event.id);
          fsm.onMessage(this, event);
        });
      } else {
        console.warn(`Agente desconocido ${event.id}. Mensaje ${JSON.stringify(event)}`);
      }
    }
    /**
     * Enviar a todos los objetos
     */
    _sendToAll(event) 
    {
      const self = this;

      this._addToQueue(() => {
        self._FSM.forEach((fsm) => fsm.onMessage(this, event))
      });
    }

    _addToQueue(action) {
      this._queue.push(action);
    }    
  }
  return new EventEmitter()
})()
package Proyecto1;

import java.util.ArrayList;
import java.util.List;

public class Subject 
{	
   private List<Observer> observers = new ArrayList<Observer>();

   public void setState(String message) 
   {
      notifyAllObservers(message);
   }

   public void attach(Observer observer)
   {
      observers.add(observer);		
   }

   public void notifyAllObservers(String message)
   {
      for(Observer observer : observers) 
      {
         observer.update(message);
      }
   } 	
}

package Proyecto1;

public class Clock 
{
    private int minutes;
    private int hours;

    public Clock() {
        this.minutes = 0;
        this.hours = 6;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
    
    public void addTime(int step)
    {
        minutes += step;
        if(minutes >= 60)
        {
            minutes = 0;
            hours++;
        }
        if(hours >= 24)
        {
            hours = 0;
        }
    }
}

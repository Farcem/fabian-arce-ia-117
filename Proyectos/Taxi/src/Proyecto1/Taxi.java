package Proyecto1;

import java.awt.Point;
import java.util.Stack;

public class Taxi extends Agent
{   
    private Point actualLocation;
    private Client client;
    private final char characterId;
    private final String color;
    private Stack<Node> dfsStack;
    private Stack<Node> clientPath;
    private boolean clientFound;
    private int overcrowdDelay;
    
    public Taxi(char characterId, Point actualLocation, FiniteStateMachine finiteStateMachine, String[] states, String[] messages, String color, Subject subject)
    {
        super(states[0], finiteStateMachine, states, messages, subject);
        this.actualLocation = actualLocation;
        this.client = null;
        this.characterId = characterId;
        this.color = color;
        this.dfsStack = null;
        this.clientPath = null;
        this.clientFound = false;
        this.overcrowdDelay = 0;
    }

    public int getOvercrowdDelay() {
        return overcrowdDelay;
    }

    public void setOvercrowdDelay(int overcrowdDelay) {
        this.overcrowdDelay = overcrowdDelay;
    }

    public void substractOvercrowdDelay() {
        this.overcrowdDelay--;
    }
    
    public Stack<Node> getClientPath() {
        return clientPath;
    }
    
    public void setClientPath(Stack<Node> clientPath) {
        this.clientPath = clientPath;
    }

    public void setDfsStack(Stack<Node> dfsStack) {
        this.dfsStack = dfsStack;
    }

    public boolean isClientFound() {
        return clientFound;
    }

    public void setClientFound(boolean clientFound) {
        this.clientFound = clientFound;
    }

    public Stack<Node> getDfsStack() {
        return dfsStack;
    }
    
    public String getColor() {
        return color;
    }
    
    public char getCharacterId() {
        return characterId;
    }

    public Point getActualLocation() {
        return actualLocation;
    }

    public void setActualLocation(Point actualLocation) {
        this.actualLocation = actualLocation;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    public boolean isDriving()
    {
        return super.getStatus().equals(super._states[0]);
    }
    public boolean isSearching()
    {
        return super.getStatus().equals(super._states[1]);
    }
    public boolean isParking()
    {
        return super.getStatus().equals(super._states[2]);
    }
    public boolean isLeaving()
    {
        return super.getStatus().equals(super._states[3]);
    }
    public Node getTaxiNode(Node[][] grid)
    {
        return grid[this.getActualLocation().x][this.getActualLocation().y];
    }

    public Node getTaxiDestinyNode(Node[][] grid)
    {
        return grid[this.getClient().getDestiny().getLocation().x][this.getClient().getDestiny().getLocation().y];
    }
}

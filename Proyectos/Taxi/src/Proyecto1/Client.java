package Proyecto1;

import java.awt.Point;

public class Client 
{
    private Building apartment;
    private Building office;
    private Point workTime;
    private boolean inWork;
    
    public Client(Building apartment, Building office, Point workTime) 
    {
        this.apartment = apartment;
        this.office = office;
        this.workTime = workTime;
        this.inWork = false;
    }

    public boolean isInWork() {
        return inWork;
    }
    
    public Building getDestiny()
    {
        if(this.isInWork())
            return this.apartment;
        else
            return this.office;
    }
    
    public void changeLocation()
    {
        this.inWork = !this.inWork;
    }
    
    public Building getCurrentBuilding()
    {
        if(this.isInWork())
            return this.getOffice();
        else
            return this.getApartment();        
    }
    
    public void setInWork(boolean inWork) {
        this.inWork = inWork;
    }
    
    public Point getWorkTime() {
        return workTime;
    }

    public boolean needsTaxi(int hour)
    {
        return this.isWorkingHour(hour) && !this.isInWork() || !this.isWorkingHour(hour) && this.isInWork();
    }
    
    public boolean isWorkingHour(int hour)
    {
        return this.workTime.x <= hour && hour < this.workTime.y;
    }
    
    public void setWorkTime(Point workTime) {
        this.workTime = workTime;
    }

    public Building getApartment() {
        return apartment;
    }

    public void setApartment(Building apartment) {
        this.apartment = apartment;
    }

    public Building getOffice() {
        return office;
    }

    public void setOffice(Building office) {
        this.office = office;
    }
}

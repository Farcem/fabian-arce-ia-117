package Proyecto1;

public class Agent extends Observer
{
    protected State _currentState;
    protected FiniteStateMachine _finiteStateMachine;
    protected String _states[];
    protected String _messages[];

    public Agent(String _currentState, FiniteStateMachine _finiteStateMachine, String[] states, String[] messages, Subject subject) 
    {
        this._finiteStateMachine = _finiteStateMachine;
        this._currentState = this._finiteStateMachine.getState(_currentState);
        this._states = states;
        this._messages = messages;
        this.subject = subject;
        this.subject.attach(this);
    }
    
    protected String getStatus()
    {
       return this._currentState.getName();
    }

    @Override
    public void update(String message) 
    {
        
        this._currentState = this._finiteStateMachine.nextState(this._currentState, message);
    }
}

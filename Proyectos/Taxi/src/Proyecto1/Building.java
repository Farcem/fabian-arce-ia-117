package Proyecto1;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class Building 
{
    private final char characterId;
    private final Point location;
    private final ArrayList<Client> clients;
    final private Queue<Client> clientsLeaving;
    final private HashSet<Client> clientsLeavingSet;

    public Building(char characterId, Point location) 
    {
        this.characterId = characterId;
        this.location = location;
        this.clients = new ArrayList<>();
        this.clientsLeaving = new LinkedList<>();
        this.clientsLeavingSet = new HashSet<>();
    }

    public ArrayList<Client> getClients() {
        return clients;
    }

    public HashSet<Client> getClientsLeavingSet() {
        return clientsLeavingSet;
    }
    
    public Queue<Client> getClientsLeaving() {
        return clientsLeaving;
    }

    public char getCharacterId() {
        return characterId;
    }

    public Point getLocation() {
        return location;
    }
}

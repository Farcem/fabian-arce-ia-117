package Proyecto1;

import java.awt.Point;
import java.util.concurrent.ThreadLocalRandom;

public class WorkSchedule 
{
    private int workTimeStart;
    private int workTimeEnd;

    private int workRange;

    public WorkSchedule(int workTimeStart, int workTimeEnd, int workRange) {
        this.workTimeStart = workTimeStart;
        this.workTimeEnd = workTimeEnd;
        this.workRange = workRange;
    }

    public int getWorkTimeStart() {
        return workTimeStart;
    }

    public void setWorkTimeStart(int workTimeStart) {
        this.workTimeStart = workTimeStart;
    }

    public int getWorkTimeEnd() {
        return workTimeEnd;
    }

    public void setWorkTimeEnd(int workTimeEnd) {
        this.workTimeEnd = workTimeEnd;
    }


    public int getWorkRange() {
        return workRange;
    }

    public void setWorkRange(int workRange) {
        this.workRange = workRange;
    }
    
    public Point getWorkTimeInterval()
    {
        int hour = ThreadLocalRandom.current().nextInt(this.workTimeStart, this.workTimeEnd + 1);
        return new Point(hour, ((hour + this.workRange)%24));
    }
}

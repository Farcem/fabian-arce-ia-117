package Proyecto1;

public class Colors 
{
    private final String ANSI_BLACK = "\u001B[30;1m";
    private final String ANSI_BLUE = "\u001B[34;1m";
    private final String ANSI_PURPLE = "\u001B[35;1m";
    private final String ANSI_CYAN = "\u001B[36;1m";
    private final String ANSI_WHITE = "\u001B[37;1m";
    
    private final String ANSI_RED_BACKGROUND = "\u001B[41m";
    private final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    private final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
 
    private final String colorList[] = {ANSI_BLACK, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN};

    private static Colors instance = null;
    protected Colors() {}

    public String getANSI_BLACK() {
        return ANSI_BLACK;
    }

    public String getANSI_BLUE() {
        return ANSI_BLUE;
    }

    public String getANSI_PURPLE() {
        return ANSI_PURPLE;
    }

    public String getANSI_CYAN() {
        return ANSI_CYAN;
    }

    public String getANSI_WHITE() {
        return ANSI_WHITE;
    }

    public String getANSI_RED_BACKGROUND() {
        return ANSI_RED_BACKGROUND;
    }

    public String getANSI_YELLOW_BACKGROUND() {
        return ANSI_YELLOW_BACKGROUND;
    }

    public String getANSI_GREEN_BACKGROUND() {
        return ANSI_GREEN_BACKGROUND;
    }

    public String[] getColorList() {
        return colorList;
    }
    
    public static Colors getInstance() 
    {
        if(instance == null) 
        {
            instance = new Colors();
        }
        return instance;
   }
}

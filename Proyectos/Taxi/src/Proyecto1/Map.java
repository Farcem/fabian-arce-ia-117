package Proyecto1;

import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Map 
{
    final private Node[][] grid;
    final private int x;
    final private int y;
    private boolean showDirection;
    private boolean showPath;
    private int animate;
    private final Clock clock;
    
    final private ArrayList<Taxi> taxis;
    final private HashMap<Character, Building> offices;
    final private HashMap<Character, Building> apartments;
    final private HashMap<Character, Building> buildings;
    private boolean client;
    private final String ANSI_RESET = "\u001B[0m";
    private double overcrowdIncrementation = 0.50;
    private double overcrowdDecrementation = 0.15;
    private int overcrowdNumber = 2;
    private final FileOutputStream log;
    private final Subject subject;
    private final ArrayList<Client> clients;

    public Map(Node[][] grid, int x, int y, ArrayList<Taxi> taxi, HashMap<Character, Building> offices, HashMap<Character, Building> apartments, HashMap<Character, Building> buildings, Subject subject, ArrayList<Client> clients) throws FileNotFoundException 
    {
        this.grid = grid;
        this.x = x;
        this.y = y;
        this.showDirection = false;
        this.showPath = false;
        this.animate = 0;
        this.taxis = taxi;
        this.offices = offices;
        this.apartments = apartments;
        this.buildings = buildings;
        this.client = false;
        this.clock = new Clock();
        this.log = new FileOutputStream(new File("taxiMap.txt"));
        this.subject = subject;
        this.clients = clients;
    }

    public Clock getClock() {
        return clock;
    }

    public ArrayList<Client> getClients() {
        return clients;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setOvercrowdIncrementation(double overcrowdIncrementation) {
        this.overcrowdIncrementation = overcrowdIncrementation;
    }

    public void setOvercrowdDecrementation(double overcrowdDecrementation) {
        this.overcrowdDecrementation = overcrowdDecrementation;
    }

    public void setOvercrowdNumber(int overcrowdNumber) {
        this.overcrowdNumber = overcrowdNumber;
    }

    public HashMap<Character, Building> getApartments() {
        return apartments;
    }

    public HashMap<Character, Building> getBuildings() {
        return buildings;
    }
    
    public boolean isClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }
    
    public boolean isShowDirection() {
        return showDirection;
    }
    
    public HashMap<Character, Building> getOffices() {
        return offices;
    }

    public void setShowDirection(boolean showDirection) {
        this.showDirection = showDirection;
    }

    public int getAnimate() {
        return animate;
    }

    public void setAnimate(int animate) {
        this.animate = animate;
    }

    public Node[][] getGrid() {
        return grid;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public ArrayList<Taxi> getTaxis() {
        return taxis;
    }

    public void setShowPath(boolean showPath) {
        this.showPath = showPath;
    }

    public boolean isShowPath() {
        return showPath;
    }
    
    public ArrayList<Node> getTaxiPossibleNextMoves(Taxi taxi, Point taxiActualLocation)
    {
        ArrayList<Node> points = new ArrayList<>();

        //Check right
        if(taxiActualLocation.y+1 < this.y && grid[taxiActualLocation.x][taxiActualLocation.y+1].isAvailableSpace(taxi))
            points.add(this.grid[taxiActualLocation.x][taxiActualLocation.y+1]);
        
        //Check down
        if(taxiActualLocation.x+1 < this.x && grid[taxiActualLocation.x+1][taxiActualLocation.y].isAvailableSpace(taxi))
            points.add(this.grid[taxiActualLocation.x+1][taxiActualLocation.y]);
        
        //Check left
        if(y-1 >= 0 && grid[taxiActualLocation.x][taxiActualLocation.y-1].isAvailableSpace(taxi))
            points.add(this.grid[taxiActualLocation.x][taxiActualLocation.y-1]);

        //Check up
        if(x-1 >= 0 && grid[taxiActualLocation.x-1][taxiActualLocation.y].isAvailableSpace(taxi))
            points.add(this.grid[taxiActualLocation.x-1][taxiActualLocation.y]);

        return points;
    }
    
    void print() throws IOException
    {
        System.out.println("Hora: " + this.clock.getHours() + ":" + String.format("%02d", this.clock.getMinutes()));
        this.log.write(("Hora: " + this.clock.getHours() + ":" + String.format("%02d", this.clock.getMinutes()) + "\n").getBytes()); 
        for(Building building : this.buildings.values())
        {
            if(building.getClients().size() > 9)
                this.grid[building.getLocation().x-1][building.getLocation().y].setCharacter('+');
            else
                this.grid[building.getLocation().x-1][building.getLocation().y].setCharacter((char)(building.getClients().size() + '0'));
            for(Client client : building.getClients())
            {
                if(client.needsTaxi(this.clock.getHours()) && !building.getClientsLeavingSet().contains(client))
                {
                    building.getClientsLeaving().add(client);
                    building.getClientsLeavingSet().add(client);
                }
                else if(!client.needsTaxi(this.clock.getHours()) && building.getClientsLeavingSet().contains(client))
                {
                    building.getClientsLeaving().remove(client);
                    building.getClientsLeavingSet().remove(client);
                }
            }
            if(building.getClientsLeaving().size() > 9)
                this.grid[building.getLocation().x+1][building.getLocation().y].setCharacter('+');
            else
                this.grid[building.getLocation().x+1][building.getLocation().y].setCharacter((char)(building.getClientsLeaving().size() + '0'));
        }
        for(int x = 0; x < this.x; x++)
        {
            for(int y = 0; y < this.y; y++)
            {
                String backgroundColor = "";

                int overcrowd = (int)this.grid[x][y].getOvercrowding();
                if((int)overcrowd >= 10)
                    backgroundColor = Colors.getInstance().getANSI_RED_BACKGROUND();
                else if((int)overcrowd >= 5)
                    backgroundColor = Colors.getInstance().getANSI_YELLOW_BACKGROUND();
                else if((int)overcrowd >= 1)
                    backgroundColor = Colors.getInstance().getANSI_GREEN_BACKGROUND();
                
                if(this.grid[x][y].getTaxiCont() == 0 && this.grid[x][y].getOvercrowding() > 0)
                {
                    if(this.grid[x][y].getOvercrowding() - overcrowdDecrementation <= 0)
                        this.grid[x][y].setOvercrowding(0);
                    else
                        this.grid[x][y].subStractOvercrowding(overcrowdDecrementation);
                }
                
                if(this.grid[x][y].getTaxiCont() > 0)
                {
                    if((int)overcrowd > 0)
                    {
                        if(overcrowd >= 10)
                        {
                            System.out.print(backgroundColor + Colors.getInstance().getANSI_WHITE() + '+' + ANSI_RESET);
                            this.log.write('+');
                        }
                        else
                        {
                            System.out.print(backgroundColor + Colors.getInstance().getANSI_WHITE() +(int)overcrowd + ANSI_RESET);
                            this.log.write(((int)overcrowd + "").getBytes());
                        }
                    }
                    else
                    {
                        System.out.print(backgroundColor + this.grid[x][y].getColor() + this.grid[x][y].getCharacter() + ANSI_RESET);
                        this.log.write((this.grid[x][y].getCharacter()+"").getBytes());
                    }    
                }
                else if((int)overcrowd > 0)
                {
                    if(overcrowd >= 10)
                    {
                        System.out.print('+');
                        this.log.write('+');
                    }
                    else
                    {
                        System.out.print((int)overcrowd);
                        this.log.write(((int)overcrowd + "").getBytes());
                    }
                }
                else if(this.showDirection && !this.grid[x][y].getPath().isEmpty())
                    System.out.print(this.grid[x][y].getColor() + "^" + ANSI_RESET);
                else if(this.showPath && !this.grid[x][y].getVisited().isEmpty())
                    System.out.print(this.grid[x][y].getColor() + "*" + ANSI_RESET);
                else
                {
                    System.out.print(this.grid[x][y].getColor() + this.grid[x][y].getCharacter() + ANSI_RESET);
                    this.log.write(this.grid[x][y].getCharacter());
                }
            }
            System.out.println();
            this.log.write("\n".getBytes());
        }
        this.log.flush();
    }
        
    public void resetGrid(Taxi taxi) 
    {
        for(int x = 0; x < this.x; x++)
        {
            for(int y = 0; y < this.y; y++)
            { 
                this.grid[x][y].getVisited().remove(taxi);
                this.grid[x][y].getExpanded().remove(taxi);
                this.grid[x][y].getPath().remove(taxi);
                this.grid[x][y].getParent().remove(taxi);
                this.grid[x][y].getCost().remove(taxi); 
                this.grid[x][y].getColors().remove(taxi);
            }
        }
    }
    
    public void resetGrid() 
    {
        for(int x = 0; x < this.x; x++)
        {
            for(int y = 0; y < this.y; y++)
            { 
                this.grid[x][y].getVisited().clear();
                this.grid[x][y].getExpanded().clear();
                this.grid[x][y].getPath().clear();
                this.grid[x][y].getParent().clear();
                this.grid[x][y].getCost().clear(); 
                this.grid[x][y].getColors().clear();
            }
        }
    }
    
    public void resetGridBools(Taxi taxi) 
    {
        for(int x = 0; x < this.x; x++)
        {
            for(int y = 0; y < this.y; y++)
            { 
                this.grid[x][y].getVisited().remove(taxi);
                this.grid[x][y].getExpanded().remove(taxi);
                this.grid[x][y].getPath().remove(taxi);
            }
        }
    }
    
    public void setHeuristicInGrid(Taxi taxi, Point destiny) 
    {
        for(int x = 0; x < this.x; x++)
            for(int y = 0; y < this.y; y++)
            {
                this.grid[x][y].setHeuristicScore(taxi, Math.abs(destiny.x - x) + Math.abs(destiny.y - y) + (int)this.grid[x][y].getOvercrowding());
            }         
    }
    
    void updateTaxi(Point newTaxiMove, Taxi taxi) 
    {
        Node actualTaxiNode = this.grid[taxi.getActualLocation().x][taxi.getActualLocation().y];
        Node newTaxiNode = this.grid[newTaxiMove.x][newTaxiMove.y];

        actualTaxiNode.setCharacter(' ');
        newTaxiNode.setCharacter(taxi.getCharacterId());

        actualTaxiNode.substractTaxiCont();
        newTaxiNode.addTaxiCont();

        if(newTaxiNode.getTaxiCont() >= overcrowdNumber)             
            newTaxiNode.addOvercrowding(overcrowdIncrementation * newTaxiNode.getTaxiCont());

        int overcrowd = (int)newTaxiNode.getOvercrowding();
        taxi.setOvercrowdDelay((int)overcrowd);

        taxi.setActualLocation(newTaxiMove);
    }
    
    public void animation() throws IOException, InterruptedException
    {
        this.print();
        if(this.getAnimate() == 0)
            System.in.read();
        else
            TimeUnit.MILLISECONDS.sleep(this.getAnimate());
        this.clock.addTime(1);
    }
}

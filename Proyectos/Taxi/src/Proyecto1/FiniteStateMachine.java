package Proyecto1;

import java.util.ArrayList;
import java.util.HashMap;

public final class FiniteStateMachine 
{
    private final HashMap<String, State> _states;

    FiniteStateMachine()
    {
        this._states = new HashMap<>();
    }

    FiniteStateMachine(ArrayList<State> states, ArrayList<Message> edges)
    {
        this._states = new HashMap<>();
        for(State state : states)
            this.addState(state);

        for(Message message : edges)
            this.addEdge(message);
    }
    
    void addState(State state)
    {
        this._states.put(state.getName(), state);
    }
    
    void addEdge(Message message)
    {
        State start = this.getState(message.start);
        State end = this.getState(message.end);
        start.add(end, message.message);
    }
    
    State getState(String name)
    {
        return this._states.get(name);
    }
    
    State nextState(State currentState, String message)
    {
        return currentState.getAdjacents().get(message);
    }
}

package Proyecto1;

import java.awt.Point;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

public class Node 
{
    private char character;
    private Point location; 
    private final HashMap<Taxi,Node> parent;
    private final HashMap<Taxi,Integer> heuristicScore;
    private final HashMap<Taxi,Integer> cost;
    private final HashSet<Taxi> expanded;
    private final HashSet<Taxi> visited;
    private final HashSet<Taxi> path;
    private final Stack<Taxi> colors;
    private int taxiCont;
    private double overcrowding;
    
    public Node(char character, Point location) 
    {
        this.colors = new Stack<>();
        this.character = character;
        this.location = location;
        this.heuristicScore = new HashMap<>();
        this.visited = new HashSet<>();
        this.expanded = new HashSet<>();
        this.path = new HashSet<>();
        this.parent = new HashMap<>();
        this.cost = new HashMap<>();
        this.taxiCont = 0;
        this.overcrowding = 0;
    }

    public int getTaxiCont() {
        return taxiCont;
    }

    public void addTaxiCont() {
        this.taxiCont++;
    }

    public void substractTaxiCont() {
        this.taxiCont--;
    }
    
    public double getOvercrowding() {
        return overcrowding;
    }

    public void setOvercrowding(double overcrowding) {
        this.overcrowding = overcrowding;
    }

    public void addOvercrowding(double overcrowding) {
        this.overcrowding += overcrowding;    
    }

    public void subStractOvercrowding(double overcrowding) {
        this.overcrowding -= overcrowding;
    }
    
    public boolean isVisited(Taxi taxi) 
    {
        return visited.contains(taxi);
    }

    public String getColor() {
        if(colors.isEmpty())
            return "";
        return colors.peek().getColor();
    }
    
    public Stack<Taxi> getColors() {
        return colors;
    }
    
    public void setVisited(Taxi taxi) {
        if(!this.visited.contains(taxi))
            this.colors.push(taxi);
        this.visited.add(taxi);
    }

    public HashSet<Taxi> getExpanded() {
        return expanded;
    }

    public void setExpanded(Taxi taxi) {
        this.expanded.add(taxi);
    }

    public boolean isPath(Taxi taxi) {
        return path.contains(taxi);
    }

    public void setPath(Taxi taxi) {
        this.colors.push(taxi);
        this.path.add(taxi);
    }

    public char getCharacter() {
        return character;
    }

    public void setCharacter(char character) {
        this.character = character;
    }

    public Node getParent(Taxi taxi) {
        return parent.get(taxi);
    }

    public void setParent(Taxi taxi, Node parent) {
        this.parent.put(taxi, parent);
    }

    public Point getLocation() {
        return location;
    }
    
    public int getCost(Taxi taxi)
    {
        return this.cost.getOrDefault(taxi, 0);
    }
    
    public void setLocation(Point location) {
        this.location = location;
    }

    public int getHeuristicScore(Taxi taxi) {
        return heuristicScore.getOrDefault(taxi,0);
    }

    public void setHeuristicScore(Taxi taxi,  int heuristicScore) 
    {
        this.heuristicScore.put(taxi, heuristicScore + this.getCost(taxi));
    }
    
    public void addCost(Taxi taxi, int cost) {
        if(this.parent.get(taxi) == null)
            this.cost.put(taxi, cost + 0);
        else
            this.cost.put(taxi, cost + this.parent.get(taxi).getCost(taxi));
    }

    public HashMap<Taxi, Node> getParent() {
        return parent;
    }

    public HashMap<Taxi, Integer> getHeuristicScore() {
        return heuristicScore;
    }

    public HashMap<Taxi, Integer> getCost() {
        return cost;
    }
    
    public HashSet<Taxi> getVisited() {
        return visited;
    }

    public HashSet<Taxi> getPath() {
        return path;
    }
    
    public boolean isAvailableSpace(Taxi taxi)
    {
        return (this.character == ' ' || this.character == taxi.getCharacterId()) && !this.visited.contains(taxi) && !this.expanded.contains(taxi);
    }
    
    public boolean isBuilding()
    {
        return Character.isAlphabetic(this.character);
    }

    public boolean isParking()
    {
        return this.character == 'x';       
    }
    
    public boolean isWall()
    {
        return this.character == '-' && this.character == '|';
    }
}

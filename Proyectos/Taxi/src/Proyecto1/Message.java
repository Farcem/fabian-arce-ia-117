package Proyecto1;

public class Message 
{
    String start;
    String message;
    String end;

    public Message(String start, String message, String end) 
    {
        this.start = start;
        this.message = message;
        this.end = end;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}

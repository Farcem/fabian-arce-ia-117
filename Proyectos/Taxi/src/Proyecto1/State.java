package Proyecto1;

import java.util.HashMap;

public class State 
{
    private String name;
    private State parent;
    private final HashMap<String, State> adjacents;

    public State(String name) 
    {
        this.name = name;
        this.adjacents = new HashMap<>();
        parent = null;
    }

    public State getParent() {
        return parent;
    }

    public void setParent(State parent) {
        this.parent = parent;
    }
    
    void add(State state, String message)
    {
        this.adjacents.put(message, state);
        state.parent = this;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, State> getAdjacents() {
        return adjacents;
    }
}

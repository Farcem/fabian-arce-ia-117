package Proyecto1;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

public class JFrameInput extends javax.swing.JFrame 
{
    public JFrameInput() 
    {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        botonPasear = new javax.swing.JButton();
        botonRuta = new javax.swing.JButton();
        botonBuscar = new javax.swing.JButton();
        botonMostrar = new javax.swing.JButton();
        textoPasear = new javax.swing.JLabel();
        textoBuscar = new javax.swing.JLabel();
        textoMostrar = new javax.swing.JLabel();
        textoRuta = new javax.swing.JLabel();
        botonAnimar = new javax.swing.JButton();
        botonClientes = new javax.swing.JButton();
        botonCliente = new javax.swing.JButton();
        botonParquear = new javax.swing.JButton();
        contadorAnimar = new javax.swing.JSpinner();
        contadorClientes = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        logLabel = new javax.swing.JLabel();
        officesComboBox = new javax.swing.JComboBox<>();
        contadorParquear1 = new javax.swing.JSpinner();
        contadorParquear2 = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        errorLabel = new javax.swing.JLabel();
        apartmentsComboBox = new javax.swing.JComboBox<>();
        overcrowdDecrementSpinner = new javax.swing.JSpinner();
        overcrowdDecrementButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        overcrowdIncrementButton = new javax.swing.JButton();
        overcrowdIncrementSpinner = new javax.swing.JSpinner();
        overcrowdNumberButton = new javax.swing.JButton();
        overcrowdNumberSpinner = new javax.swing.JSpinner();
        addTaxiButton = new javax.swing.JButton();
        workTimeStartSpinner = new javax.swing.JSpinner();
        workTimeEndSpinner = new javax.swing.JSpinner();
        workTimeButton = new javax.swing.JButton();

        jTextField1.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        botonPasear.setText("Pasear");
        botonPasear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonPasearActionPerformed(evt);
            }
        });

        botonRuta.setText("Ruta");

        botonBuscar.setText("Buscar");

        botonMostrar.setText("Mostrar");

        textoPasear.setBackground(java.awt.Color.red);
        textoPasear.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        textoPasear.setOpaque(true);

        textoBuscar.setBackground(java.awt.Color.red);
        textoBuscar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        textoBuscar.setOpaque(true);

        textoMostrar.setBackground(java.awt.Color.red);
        textoMostrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        textoMostrar.setOpaque(true);

        textoRuta.setBackground(java.awt.Color.red);
        textoRuta.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        textoRuta.setOpaque(true);

        botonAnimar.setText("Animar");
        botonAnimar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAnimarActionPerformed(evt);
            }
        });

        botonClientes.setText("Clientes");

        botonCliente.setText("Cliente");

        botonParquear.setText("Parquear");

        contadorAnimar.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));

        contadorClientes.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

        jLabel1.setText("Error");

        logLabel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        officesComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        contadorParquear1.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));

        contadorParquear2.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));

        jLabel3.setText("Estado:");

        errorLabel.setForeground(java.awt.Color.red);
        errorLabel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        apartmentsComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        overcrowdDecrementSpinner.setModel(new javax.swing.SpinnerNumberModel(0.1d, 0.1d, null, 0.1d));

        overcrowdDecrementButton.setText("Decremento");
        overcrowdDecrementButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                overcrowdDecrementButtonActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Congestionamiento");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        overcrowdIncrementButton.setText("Incremento");
        overcrowdIncrementButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                overcrowdIncrementButtonActionPerformed(evt);
            }
        });

        overcrowdIncrementSpinner.setModel(new javax.swing.SpinnerNumberModel(0.25d, 0.25d, null, 0.25d));

        overcrowdNumberButton.setText("Cantidad");
        overcrowdNumberButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                overcrowdNumberButtonActionPerformed(evt);
            }
        });

        overcrowdNumberSpinner.setModel(new javax.swing.SpinnerNumberModel(2, 1, null, 1));

        addTaxiButton.setText("Agregar taxi");
        addTaxiButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addTaxiButtonActionPerformed(evt);
            }
        });

        workTimeStartSpinner.setModel(new javax.swing.SpinnerNumberModel(7, 0, 23, 1));

        workTimeEndSpinner.setModel(new javax.swing.SpinnerNumberModel(9, 0, 23, 1));

        workTimeButton.setText("Horas de trabajo");
        workTimeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                workTimeButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonBuscar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonRuta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonMostrar)
                            .addComponent(botonPasear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(textoRuta, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(textoBuscar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(textoMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textoPasear, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(botonParquear)
                                .addGap(18, 18, 18)
                                .addComponent(contadorParquear1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(contadorParquear2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(botonClientes, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                                    .addComponent(botonCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(botonAnimar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(apartmentsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(officesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(contadorAnimar)
                                    .addComponent(contadorClientes)))))
                    .addComponent(logLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3)
                    .addComponent(errorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(overcrowdDecrementSpinner, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(overcrowdIncrementSpinner, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(overcrowdIncrementButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(overcrowdDecrementButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(overcrowdNumberButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(overcrowdNumberSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(115, 115, 115))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(addTaxiButton)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(workTimeStartSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(28, 28, 28)
                                        .addComponent(workTimeEndSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(workTimeButton))))
                        .addContainerGap(28, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(textoPasear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(botonPasear, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(botonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(2, 2, 2)
                                                .addComponent(textoBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(botonRuta, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                                            .addComponent(textoRuta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(botonCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(officesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(apartmentsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(16, 16, 16)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(botonClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(contadorClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(botonAnimar, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                                .addComponent(contadorAnimar)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textoMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(botonParquear, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(contadorParquear1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(contadorParquear2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(overcrowdDecrementButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(overcrowdDecrementSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(overcrowdIncrementButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(overcrowdIncrementSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(overcrowdNumberButton, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(overcrowdNumberSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(logLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addTaxiButton)
                            .addComponent(workTimeButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(workTimeEndSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(workTimeStartSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(errorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        botonPasear.getAccessibleContext().setAccessibleDescription("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAnimarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAnimarActionPerformed
        
    }//GEN-LAST:event_botonAnimarActionPerformed

    private void botonPasearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonPasearActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botonPasearActionPerformed

    private void overcrowdDecrementButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_overcrowdDecrementButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_overcrowdDecrementButtonActionPerformed

    private void overcrowdIncrementButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_overcrowdIncrementButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_overcrowdIncrementButtonActionPerformed

    private void overcrowdNumberButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_overcrowdNumberButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_overcrowdNumberButtonActionPerformed

    private void addTaxiButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addTaxiButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addTaxiButtonActionPerformed

    private void workTimeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_workTimeButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_workTimeButtonActionPerformed

    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameInput().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addTaxiButton;
    private javax.swing.JComboBox<String> apartmentsComboBox;
    private javax.swing.JButton botonAnimar;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonCliente;
    private javax.swing.JButton botonClientes;
    private javax.swing.JButton botonMostrar;
    private javax.swing.JButton botonParquear;
    private javax.swing.JButton botonPasear;
    private javax.swing.JButton botonRuta;
    private javax.swing.JSpinner contadorAnimar;
    private javax.swing.JSpinner contadorClientes;
    private javax.swing.JSpinner contadorParquear1;
    private javax.swing.JSpinner contadorParquear2;
    private javax.swing.JLabel errorLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel logLabel;
    private javax.swing.JComboBox<String> officesComboBox;
    private javax.swing.JButton overcrowdDecrementButton;
    private javax.swing.JSpinner overcrowdDecrementSpinner;
    private javax.swing.JButton overcrowdIncrementButton;
    private javax.swing.JSpinner overcrowdIncrementSpinner;
    private javax.swing.JButton overcrowdNumberButton;
    private javax.swing.JSpinner overcrowdNumberSpinner;
    private javax.swing.JLabel textoBuscar;
    private javax.swing.JLabel textoMostrar;
    private javax.swing.JLabel textoPasear;
    private javax.swing.JLabel textoRuta;
    private javax.swing.JButton workTimeButton;
    private javax.swing.JSpinner workTimeEndSpinner;
    private javax.swing.JSpinner workTimeStartSpinner;
    // End of variables declaration//GEN-END:variables

    public JButton getBotonAnimar() {
        return botonAnimar;
    }

    public JButton getWorkTimeButton() {
        return workTimeButton;
    }

    public JSpinner getWorkTimeEndSpinner() {
        return workTimeEndSpinner;
    }

    public JSpinner getWorkTimeStartSpinner() {
        return workTimeStartSpinner;
    }

    public JButton getAddTaxiButton() {
        return addTaxiButton;
    }

    public JComboBox<String> getApartmentsComboBox() {
        return apartmentsComboBox;
    }

    public JComboBox<String> getOfficesComboBox() {
        return officesComboBox;
    }

    public JSpinner getContadorParquear1() {
        return contadorParquear1;
    }

    public void setContadorParquear1(JSpinner contadorParquear1) {
        this.contadorParquear1 = contadorParquear1;
    }

    public JSpinner getContadorParquear2() {
        return contadorParquear2;
    }

    public void setContadorParquear2(JSpinner contadorParquear2) {
        this.contadorParquear2 = contadorParquear2;
    }

    public void setBotonAnimar(JButton botonAnimar) {
        this.botonAnimar = botonAnimar;
    }

    public JButton getBotonBuscar() {
        return botonBuscar;
    }

    public void setBotonBuscar(JButton botonBuscar) {
        this.botonBuscar = botonBuscar;
    }

    public JButton getBotonCliente() {
        return botonCliente;
    }
    
    public void setBotonCliente(JButton botonCliente) {
        this.botonCliente = botonCliente;
    }

    public JButton getBotonClientes() {
        return botonClientes;
    }

    public void setBotonClientes(JButton botonClientes) {
        this.botonClientes = botonClientes;
    }

    public JButton getBotonMostrar() {
        return botonMostrar;
    }

    public void setBotonMostrar(JButton botonMostrar) {
        this.botonMostrar = botonMostrar;
    }

    public JButton getBotonParquear() {
        return botonParquear;
    }

    public void setBotonParquear(JButton botonParquear) {
        this.botonParquear = botonParquear;
    }

    public JButton getBotonPasear() {
        return botonPasear;
    }

    public void setBotonPasear(JButton botonPasear) {
        this.botonPasear = botonPasear;
    }

    public JButton getBotonRuta() {
        return botonRuta;
    }

    public void setBotonRuta(JButton botonRuta) {
        this.botonRuta = botonRuta;
    }

    public JSpinner getContadorAnimar() {
        return contadorAnimar;
    }

    public void setContadorAnimar(JSpinner contadorAnimar) {
        this.contadorAnimar = contadorAnimar;
    }

    public JSpinner getContadorClientes() {
        return contadorClientes;
    }

    public void setContadorClientes(JSpinner contadorClientes) {
        this.contadorClientes = contadorClientes;
    }

    public JPanel getjPanel1() {
        return jPanel1;
    }

    public void setjPanel1(JPanel jPanel1) {
        this.jPanel1 = jPanel1;
    }

    public JTextField getjTextField1() {
        return jTextField1;
    }

    public void setjTextField1(JTextField jTextField1) {
        this.jTextField1 = jTextField1;
    }

    public JLabel getTextoBuscar() {
        return textoBuscar;
    }

    public void setTextoBuscar(JLabel textoBuscar) {
        this.textoBuscar = textoBuscar;
    }

    public JLabel getTextoMostrar() {
        return textoMostrar;
    }

    public void setTextoMostrar(JLabel textoMostrar) {
        this.textoMostrar = textoMostrar;
    }

    public JLabel getTextoPasear() {
        return textoPasear;
    }

    public void setTextoPasear(JLabel textoPasear) {
        this.textoPasear = textoPasear;
    }

    public JLabel getTextoRuta() {
        return textoRuta;
    }

    public void setTextoRuta(JLabel textoRuta) {
        this.textoRuta = textoRuta;
    }

    public JLabel getLogLabel() {
        return logLabel;
    }

    public void setLogLabel(JLabel logLabel) {
        this.logLabel = logLabel;
    }

    public JLabel getErrorLabel() {
        return errorLabel;
    }

    public void setErrorLabel(JLabel errorLabel) {
        this.errorLabel = errorLabel;
    }

    public JButton getOvercrowdDecrementButton() {
        return overcrowdDecrementButton;
    }

    public JSpinner getOvercrowdDecrementSpinner() {
        return overcrowdDecrementSpinner;
    }

    public JButton getOvercrowdIncrementButton() {
        return overcrowdIncrementButton;
    }

    public JSpinner getOvercrowdIncrementSpinner() {
        return overcrowdIncrementSpinner;
    }

    public JButton getOvercrowdNumberButton() {
        return overcrowdNumberButton;
    }

    public JSpinner getOvercrowdNumberSpinner() {
        return overcrowdNumberSpinner;
    }
    

}

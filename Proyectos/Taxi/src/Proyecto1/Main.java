package Proyecto1;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;
import javax.swing.DefaultComboBoxModel;

public class Main
{
    static char taxiSymbol = '•';
    
    static Random random = new Random();
    static Map map;
    static JFrameInput frame = new JFrameInput();
    
    static String states[] = {"Paseando","Buscando","Parqueando","Dejando cliente"};
    static String messages[] = {"Drive","Search","Park","Leave"};
    
    static WorkSchedule workSchedule = new WorkSchedule(7,9, 8);
    
    static boolean newTaxi = false;
    static boolean errors = false;
  
    
    static String drive()
    {
        return messages[0];
    }
    static String search()
    {
        return messages[1];
    }
    static String park()
    {
        return messages[2];
    }
    static String leave()
    {
        return messages[3];
    }
    
    public static class ThreadInput extends Thread 
    {
        public void run() 
        {
            frame.setVisible(true);
            frame.getTextoPasear().setBackground(Color.green);
            String officesId[] = new String[map.getOffices().size()];
            String apartmentsId[] = new String[map.getApartments().size()];

            int cont = 0;
            for(Building building : map.getOffices().values())
                officesId[cont++] = "" + building.getCharacterId();

            cont = 0;
            for(Building building : map.getApartments().values())
                apartmentsId[cont++] = "" + building.getCharacterId();

            Arrays.sort(apartmentsId);
            Arrays.sort(officesId);
            
            frame.getOfficesComboBox().setModel(new DefaultComboBoxModel(officesId));
            frame.getApartmentsComboBox().setModel(new DefaultComboBoxModel(apartmentsId));
            
            frame.getBotonAnimar().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    map.setAnimate((Integer)frame.getContadorAnimar().getValue());
                };
            });
            frame.getBotonParquear().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    //map.getTaxi().updateAgent(park());
                };
            });
            frame.getBotonBuscar().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    map.resetGrid();
                    map.getSubject().setState(search());
                }
            });
            frame.getBotonPasear().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    map.resetGrid();
                    map.getSubject().setState(drive());
                }
            });
            frame.getBotonMostrar().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    if(map.isShowPath())
                    {
                        frame.getTextoMostrar().setBackground(Color.red);
                        map.setShowPath(false);
                    }
                    else
                    {
                        frame.getTextoMostrar().setBackground(Color.green);
                        map.setShowPath(true);
                    }
                }
            });
            frame.getBotonRuta().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    if(map.isShowDirection())
                    {
                        frame.getTextoRuta().setBackground(Color.red);
                        map.setShowDirection(false);
                    }
                    else
                    {
                        frame.getTextoRuta().setBackground(Color.green);
                        map.setShowDirection(true);
                    }
                }
            });
            frame.getBotonClientes().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    int clients = (Integer)frame.getContadorClientes().getValue();
                    while(clients-- > 0)
                    {
                        Building apartment = (Building) map.getApartments().values().toArray()[random.nextInt(map.getApartments().size())];
                        Building office = (Building) map.getOffices().values().toArray()[random.nextInt(map.getOffices().size())];
                        
                        apartment.getClients().add(new Client(apartment, office, workSchedule.getWorkTimeInterval()));
                    }
                    frame.getErrorLabel().setText("");
                    frame.getLogLabel().setText("Se han agregado los clientes de forma aleatoria al mapa");
                }
            });
            frame.getBotonCliente().addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    char officeId = ((String)frame.getOfficesComboBox().getSelectedItem()).charAt(0);
                    char apartmentId = ((String)frame.getApartmentsComboBox().getSelectedItem()).charAt(0); 
                    map.getApartments().get(apartmentId).getClients().add(new Client(map.getApartments().get(apartmentId), map.getOffices().get(officeId), workSchedule.getWorkTimeInterval()));
                    frame.getErrorLabel().setText("");
                    frame.getLogLabel().setText("Se agrego al nuevo cliente");
                }
            });
            frame.getOvercrowdDecrementButton().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    map.setOvercrowdDecrementation((double)frame.getOvercrowdDecrementSpinner().getValue()); 
                }
            });
            frame.getOvercrowdIncrementButton().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    map.setOvercrowdIncrementation((double)frame.getOvercrowdIncrementSpinner().getValue()); 
                }
            });
            frame.getOvercrowdNumberButton().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    map.setOvercrowdNumber((int)frame.getOvercrowdNumberSpinner().getValue()); 
                }
            });
            frame.getAddTaxiButton().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    newTaxi = true;
                }
            });
            frame.getWorkTimeButton().addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    int start = (int)frame.getWorkTimeStartSpinner().getValue();
                    int end = (int)frame.getWorkTimeEndSpinner().getValue();

                    workSchedule.setWorkTimeStart(start);
                    workSchedule.setWorkTimeEnd(end);
                    
                    for(Client client : map.getClients())
                    {
                        client.setWorkTime(workSchedule.getWorkTimeInterval());
                    } 
                }
            });
        }
    }
    
    public static void taxiDriving(Taxi taxi) throws IOException
    {
        frame.getTextoPasear().setBackground(Color.green);
        frame.getTextoBuscar().setBackground(Color.red);
        frame.getErrorLabel().setText("");
        frame.getLogLabel().setText("El taxi esta paseando...");
        ArrayList<Node> taxiPossibleNextMoves = map.getTaxiPossibleNextMoves(taxi, taxi.getActualLocation());
        
        if(taxiPossibleNextMoves.isEmpty())
            map.resetGrid(taxi);
        else
        {
            Point newTaxiMove = taxiPossibleNextMoves.get(random.nextInt(taxiPossibleNextMoves.size())).getLocation();
            map.updateTaxi(newTaxiMove, taxi);            
            map.getGrid()[taxi.getActualLocation().x][taxi.getActualLocation().y].setVisited(taxi);
        }
    }
  
    private static Node getMinPuzzleHeuristicScore(Taxi taxi, ArrayList<Node> moves)
    {
        int minHeuristicScore = Integer.MAX_VALUE;
        Node node = null;
        for(Node move : moves)
        {
            int score = move.getHeuristicScore(taxi);
            if(score < minHeuristicScore)
            {
                minHeuristicScore = score;
                node = move;
            }
        }
        return node;
    }  
    
    public static Stack<Node> aStarDestinySearch(Taxi taxi, Node destiny) throws IOException, InterruptedException
    {
        map.resetGrid(taxi); 
        map.setHeuristicInGrid(taxi, destiny.getLocation());
        
        ArrayList<Node> openList = new ArrayList<>();
        ArrayList<Node> possibleNextMoves;
        
        openList.add(taxi.getTaxiNode(map.getGrid()));
        Node actualNode = null;
        
        Boolean found = false;
        
        while(!openList.isEmpty())
        {
            actualNode = getMinPuzzleHeuristicScore(taxi, openList);
            openList.remove(actualNode);
            actualNode.setVisited(taxi);
            
            if(isDestination(actualNode.getLocation(), destiny.getLocation(), taxi))
            {
                found = true;
                break;
            }
            possibleNextMoves = map.getTaxiPossibleNextMoves(taxi, actualNode.getLocation());
            for(Node move : possibleNextMoves)
            {                            
                if(!move.isVisited(taxi) && !openList.contains(move))
                {
                    move.setParent(taxi, actualNode);
                    move.addCost(taxi, 1); 
                    openList.add(move);
                }
            }
        }
        if(found)
        {
            map.resetGridBools(taxi);        
            Stack<Node> path = new Stack<>();
            
            while(actualNode != null && !actualNode.getLocation().equals(taxi.getActualLocation()))
            {
                map.getGrid()[actualNode.getLocation().x][actualNode.getLocation().y].setPath(taxi); 
                path.add(actualNode);
                actualNode = actualNode.getParent(taxi);
            }
             
            return path;
        }
        else
        {
            frame.getErrorLabel().setText("No hay forma de llegar a ese destino");
            return null;
        }
    }
    
    public static void dfsClientSearch(Taxi taxi) throws IOException, InterruptedException
    {
        if(taxi.getDfsStack() == null)
        {
            frame.getTextoPasear().setBackground(Color.red);
            frame.getTextoBuscar().setBackground(Color.green);
            frame.getErrorLabel().setText("");
            frame.getLogLabel().setText("El taxi esta buscando...");
            taxi.setDfsStack(new Stack<>());
            map.resetGrid(taxi);
            taxi.getDfsStack().add(taxi.getTaxiNode(map.getGrid()));
        }
        
        Node actualNode = taxi.getDfsStack().pop();
        actualNode.setVisited(taxi);
        map.updateTaxi(actualNode.getLocation(), taxi);
        Client client = getClientNearby(actualNode.getLocation());
        if(client != null)
        {
            taxi.setClient(client);
            taxi.setClientFound(true);
        }

        if(!taxi.isClientFound())
        {
            for(Node child : map.getTaxiPossibleNextMoves(taxi, actualNode.getLocation()))
            {
                child.setExpanded(taxi);
                child.setParent(taxi, actualNode);
                taxi.getDfsStack().add(child);
            }
        }
    }
    
    public static void taxiSearching(Taxi taxi) throws IOException, InterruptedException
    {
        if((taxi.getDfsStack() == null || !taxi.getDfsStack().isEmpty()) && !taxi.isClientFound())
        {
            dfsClientSearch(taxi);
        }
        
        else if(taxi.getClientPath() == null && taxi.isClientFound())
        {
            frame.getLogLabel().setText("Cliente encontrado, destino a: " + taxi.getClient().getDestiny().getCharacterId());
            taxi.setClientPath(aStarDestinySearch(taxi, taxi.getTaxiDestinyNode(map.getGrid())));         
        }
        else if(taxi.getClientPath() != null && !taxi.getClientPath().isEmpty())
        {
            map.updateTaxi(taxi.getClientPath().pop().getLocation(), taxi);
            taxi.getTaxiNode(map.getGrid()).setVisited(taxi);
            taxi.setClientPath(aStarDestinySearch(taxi, taxi.getTaxiDestinyNode(map.getGrid())));
        }
        else if(taxi.isClientFound())
        {
            frame.getLogLabel().setText("El taxi llego al destino " + taxi.getClient().getDestiny().getCharacterId());     
            map.resetGrid(taxi);
            taxi.getClient().getDestiny().getClients().add(taxi.getClient());
            taxi.getClient().changeLocation();
            taxi.setClientFound(false);
            taxi.setClient(null); 
            taxi.setClientPath(null);
            taxi.setDfsStack(null); 
        }
        else
        {
            map.resetGrid(taxi);
            taxi.setDfsStack(null);
        }
    }
    
    private static void taxiParking(Taxi taxi) throws IOException, InterruptedException 
    {
        Point parkingLocation = new Point((Integer)frame.getContadorParquear1().getValue(),(Integer)frame.getContadorParquear2().getValue());
        if(parkingLocation.x < map.getX() && parkingLocation.y < map.getY())
        {
            if(map.getGrid()[parkingLocation.x][parkingLocation.y].getCharacter() == '-')
            {
                map.getGrid()[parkingLocation.x][parkingLocation.y].setCharacter('x');
                frame.getErrorLabel().setText("");
                frame.getLogLabel().setText("El taxi esta parqueando...");
                aStarDestinySearch(taxi, map.getGrid()[parkingLocation.x][parkingLocation.y]);
                frame.getLogLabel().setText("El taxi llego al punto de parqueo");
    
                map.getGrid()[parkingLocation.x][parkingLocation.y].setCharacter('-');

            }
            else
            {
                frame.getErrorLabel().setText("No es una acera o hay un cliente");
            }
        }
        else
        {
            frame.getErrorLabel().setText("Esa localizacion no es valida");
        } 
    }
    
    public static Client getClientNearby(Point location)
    {
        char destiny = ' ';
        
        //Check up
        if(location.x-2 >= 0 && map.getGrid()[location.x-2][location.y].isBuilding())
        {
            destiny = map.getGrid()[location.x-2][location.y].getCharacter();
        }
        //Check left
        else if(location.y-2 >= 0 && map.getGrid()[location.x][location.y-2].isBuilding())
        { 
            destiny = map.getGrid()[location.x][location.y-2].getCharacter();
        }
        //Check right
        else if(location.y+2 < map.getY() && map.getGrid()[location.x][location.y+2].isBuilding())
        { 
            destiny = map.getGrid()[location.x][location.y+2].getCharacter();
        }
        //Check down
        else if(location.x+2 < map.getX() && map.getGrid()[location.x+2][location.y].isBuilding())
        { 
            destiny = map.getGrid()[location.x+2][location.y].getCharacter();
        }
        if(destiny == ' ')
            return null;
        
        if(!map.getBuildings().get(destiny).getClientsLeaving().isEmpty())
        {
            Client leavingClient = map.getBuildings().get(destiny).getClientsLeaving().remove();
            map.getBuildings().get(destiny).getClients().remove(leavingClient);
            map.getBuildings().get(destiny).getClientsLeavingSet().remove(leavingClient);
            return leavingClient;
        }
        else
            return null;
    }
    
    public static boolean isDestination(Point taxiLocation, Point destiny, Taxi taxi)
    { 
        //Check up
        if(taxiLocation.x-2 >= 0 && ((map.getGrid()[taxiLocation.x-2][taxiLocation.y].getLocation().equals(destiny) && !taxi.isParking()) || map.getGrid()[taxiLocation.x-1][taxiLocation.y].isParking()))
        {
            return true;
        }
        //Check left
        else if(taxiLocation.y-2 >= 0 && ((map.getGrid()[taxiLocation.x][taxiLocation.y-2].getLocation().equals(destiny) && !taxi.isParking()) || map.getGrid()[taxiLocation.x][taxiLocation.y-1].isParking()))
        { 
            return true;
        }
        //Check right
        else if(taxiLocation.y+2 < map.getY() && ((map.getGrid()[taxiLocation.x][taxiLocation.y+2].getLocation().equals(destiny) && !taxi.isParking()) || map.getGrid()[taxiLocation.x][taxiLocation.y+1].isParking()))
        { 
            return true;
        }
        //Check down
        else if(taxiLocation.x+2 < map.getX() && ((map.getGrid()[taxiLocation.x+2][taxiLocation.y].getLocation().equals(destiny) && !taxi.isParking()) || map.getGrid()[taxiLocation.x+1][taxiLocation.y].isParking()))
        { 
            return true;
        }
        else
            return false;
    }
    
    static FiniteStateMachine taxiBehavior(String states[], String messages[]) 
    {
        FiniteStateMachine finiteStringMachine = new FiniteStateMachine();
        
        for(String state : states)
        {
            finiteStringMachine.addState(new State(state));
        }
        ArrayList<Message> messageList = new ArrayList<>();
        
        messageList.add(new Message(states[0], messages[0], states[0]));
        messageList.add(new Message(states[0], messages[1], states[1]));
        messageList.add(new Message(states[0], messages[2], states[2]));
        messageList.add(new Message(states[0], messages[3], states[3]));

        messageList.add(new Message(states[1], messages[0], states[0]));
        messageList.add(new Message(states[1], messages[1], states[1]));
        messageList.add(new Message(states[1], messages[2], states[2]));
        messageList.add(new Message(states[1], messages[3], states[3]));

        messageList.add(new Message(states[2], messages[0], states[0]));
        messageList.add(new Message(states[2], messages[1], states[1]));
        messageList.add(new Message(states[2], messages[2], states[2]));
        messageList.add(new Message(states[2], messages[3], states[3]));

        messageList.add(new Message(states[3], messages[0], states[0]));
        messageList.add(new Message(states[3], messages[1], states[1]));
        messageList.add(new Message(states[3], messages[2], states[2]));
        messageList.add(new Message(states[3], messages[3], states[3]));
           
        for(Message message : messageList)
            finiteStringMachine.addEdge(message);
        return finiteStringMachine;
    }
    
    static Taxi createTaxiAgent(Point taxiLocation, char taxiCharacter, Subject subject)
    {   
        return new Taxi(taxiCharacter, taxiLocation, taxiBehavior(states, messages), states, messages, Colors.getInstance().getColorList()[random.nextInt(Colors.getInstance().getColorList().length)], subject);
    }
    
    public static void main(String[] args) throws FileNotFoundException, InterruptedException, IOException
    {
        //READ THE MAP CHARACTERS AND LOCATIONS IN THE MAP.TXT (x,y)
        File textMap = new File("Map.txt");
        Scanner scanner = new Scanner(textMap);
        
        HashMap<Character,Building> apartments = new HashMap<>();
        HashMap<Character,Building> offices = new HashMap<>(); 
        HashMap<Character,Building> buildings = new HashMap<>();
        ArrayList<Taxi> taxis = new ArrayList<>();
        ArrayList<Client> clients = new ArrayList<>();
        int gridX = scanner.nextInt();
        int gridY = scanner.nextInt();
        
        Node[][] grid = new Node[gridX][gridY];
        String row;
        scanner.nextLine();
        Subject subject = new Subject();
        
        for(int x = 0; x < gridX; x++)
        {
            row = scanner.nextLine();
            for(int y = 0; y < gridY; y++)
            {
                if(row.charAt(y) == taxiSymbol) 
                {
                    taxis.add(createTaxiAgent(new Point(x,y), row.charAt(y), subject));
                }
                else if(Character.isUpperCase(row.charAt(y)))
                {
                    Building apartment = new Building(row.charAt(y), new Point(x,y));
                    apartments.put(row.charAt(y), apartment);
                    buildings.put(row.charAt(y), apartment);
                }
                else if(Character.isLowerCase(row.charAt(y)))
                {
                    Building office = new Building(row.charAt(y), new Point(x,y));
                    offices.put(row.charAt(y), office);
                    buildings.put(row.charAt(y), office);
                }
                
                grid[x][y] = new Node(row.charAt(y), new Point(x,y));
            }
        }
        
        for(Building building : apartments.values())
        {
            if(Character.isDigit(grid[building.getLocation().x-1][building.getLocation().y].getCharacter()))
            {
                int peopleInApartment = Character.getNumericValue(grid[building.getLocation().x-1][building.getLocation().y].getCharacter());
                for(int i = 0 ; i < peopleInApartment; i++)
                {
                    Object officesArray[] = offices.values().toArray();
                    Client client = new Client(building, (Building)officesArray[random.nextInt(offices.size())], workSchedule.getWorkTimeInterval());
                    building.getClients().add(client);
                    clients.add(client);
                }
            }
        }
        
        for(Taxi taxi : taxis)
        {
            grid[taxi.getActualLocation().x][taxi.getActualLocation().y].setVisited(taxi);
            grid[taxi.getActualLocation().x][taxi.getActualLocation().y].addTaxiCont();
        }
        
        if(taxis.isEmpty() || apartments.isEmpty() || offices.isEmpty())
            errors = true;
        //FINISH READING MAP.TXT
        //####################################
        
        //####################################
        
        //CREATE MAP WITH CHARACTERS, LOCATIONS AND AGENTS
        map = new Map(grid, gridX, gridY, taxis, offices, apartments, buildings, subject, clients);
        
        //INIT THE MAIN LOOP

        (new Thread(new ThreadInput())).start();
        
        
        while(!errors)
        {
            map.animation();
            for(Taxi actualTaxi : map.getTaxis())
            {
                if(actualTaxi.getOvercrowdDelay() > 0)
                    actualTaxi.substractOvercrowdDelay();
                else
                {
                    if(actualTaxi.isParking())
                    {
                        taxiParking(actualTaxi);
                    }
                    else if(actualTaxi.isDriving())
                    {
                        taxiDriving(actualTaxi);
                    }
                    else if(actualTaxi.isSearching())
                    {
                        taxiSearching(actualTaxi);
                    }
                }
            }
            if(newTaxi)
            {
                taxis.add(createTaxiAgent(new Point(1,1), taxiSymbol, subject));
                newTaxi = false;
            }
        }
        frame.getErrorLabel().setText("Verifique que en el mapa hay taxis, destinos y aceras"); 
    }
}

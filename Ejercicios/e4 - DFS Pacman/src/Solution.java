import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Solution
{
    public static class Matrix
    {
        private final int X;
        private final int Y;
        private final Cell[][] data;

        public Matrix(Cell[][] data)
        {
            X = data.length;
            Y = data[0].length;
            this.data = data;
        }

        public int getX() {
            return X;
        }

        public int getY() {
            return Y;
        }

        public void print() 
        {
            for (int i = 0; i < X; i++) 
            {
                for (int j = 0; j < Y; j++) 
                    System.out.print(data[i][j].character + " - " + data[i][j].visited + "|");
                System.out.println();
            }
        }

        public Cell getCell(int x, int y)
        {
            return this.data[x][y];
        }
    }
    
    public static class Cell 
    {
        private char character;
        private boolean visited = false;
        private boolean added = false;        
        private final Point location;
        private Cell parent;

        public Cell(char character, Point location)
        {
            this.character = character;
            this.location = location;
        }

        public Cell getParent() {
            return parent;
        }

        public void setParent(Cell parent) {
            this.parent = parent;
        }

        public Point getLocation() {
            return location;
        }
        
        public char getCharacter() {
            return character;
        }

        public void setCharacter(char character) {
            this.character = character;
        }
        public boolean isVisited() {
            return visited;
        }

        public void setVisited(boolean visited) {
            this.visited = visited;
        }

        public boolean isAdded() {
            return added;
        }

        public void setAdded(boolean added) {
            this.added = added;
        }
    }

    public static void depthFirstSearch(Matrix grid, Point Pacman, Point Food) throws IOException
    {
        Stack<Point> stack = new Stack<>();
        ArrayList<Point> points = new ArrayList<>();
        Queue<Point> unvisitedNeighbours;
        Point location;
        
        stack.push(Pacman);
        
        while(!stack.isEmpty())
        {
            location = stack.pop();

            unvisitedNeighbours = getUnvisitedNeighbours(grid, location.x, location.y);           

            grid.getCell(location.x, location.y).setVisited(true);
            points.add(location);
            
            if(location.equals(Food))
            {
                grid.getCell(Food.x, Food.y).setParent(grid.getCell(location.x, location.y).getParent());
                break;
            }

            while(!unvisitedNeighbours.isEmpty())
            {
                grid.getCell(unvisitedNeighbours.peek().x, unvisitedNeighbours.peek().y).setParent(grid.getCell(location.x, location.y));
                grid.getCell(unvisitedNeighbours.peek().x, unvisitedNeighbours.peek().y).setAdded(true);

                stack.add(unvisitedNeighbours.poll());
            }
        }
        
        System.out.println(points.size());
        for(Point point : points)
            System.out.println(point.x + " " + point.y);
        
        Stack<Point> path = new Stack<>();
        Cell tarjet = grid.getCell(Food.x, Food.y);
        
        while(!tarjet.getLocation().equals(Pacman))
        {
            path.add(tarjet.getLocation());
            tarjet = tarjet.getParent();
        }
        System.out.println(path.size());
        path.add(Pacman);
        
        while(!path.isEmpty())
        {
            System.out.println(path.peek().x + " " + path.peek().y);
            path.pop();
        }
    }
    
    public static Queue<Point> getUnvisitedNeighbours(Matrix grid, int x, int y)
    {
        Queue<Point> points = new LinkedList<>();
        //Check up
        if(x-1 >= 0 && !grid.getCell(x-1, y).isVisited() && !grid.getCell(x-1, y).isAdded() && grid.getCell(x-1, y).getCharacter() != '%')
            points.add(new Point(x-1, y));

        //Check left
        if(y-1 >= 0 && !grid.getCell(x, y-1).isVisited() && !grid.getCell(x, y-1).isAdded() && grid.getCell(x, y-1).getCharacter() != '%')
            points.add(new Point(x, y-1));

        //Check right
        if(y+1 < grid.getY() && !grid.getCell(x, y+1).isVisited() && !grid.getCell(x, y+1).isAdded() && grid.getCell(x, y+1).getCharacter() != '%')
            points.add(new Point(x, y+1));

        //Check down
        if(x+1 < grid.getX() && !grid.getCell(x+1, y).isVisited() && !grid.getCell(x+1, y).isAdded() && grid.getCell(x+1, y).getCharacter() != '%')
            points.add(new Point(x+1, y));
        
        return points;
    }  
    
    public static void main(String[] args) throws IOException 
    {
        Scanner in = new Scanner(System.in);

        int pacman_x = in.nextInt(); //The first line contains 2 space separated integers which is the position of the PacMan. 
        int pacman_y = in.nextInt(); 

        int food_x = in.nextInt(); //The second line contains 2 space separated integers which is the position of the food. 
        int food_y = in.nextInt();

        int grid_x = in.nextInt(); //The third line of the input contains 2 space separated integers indicating the size of the rows and columns 
        int grid_y = in.nextInt();
    
        Cell[][] grid = new Cell[grid_x][grid_y];
        
        String row;
        for(int x = 0; x < grid_x; x++) 
        {
            row = in.next();
            
            for(int y = 0; y < grid_y; y++)
                grid[x][y] = new Cell(row.charAt(y), new Point(x,y));
        }
        
        Point pacman = new Point(pacman_x, pacman_y);
        Point food = new Point(food_x, food_y);
        Matrix gridCell = new Matrix(grid);
        
       depthFirstSearch(gridCell, pacman, food);
    }
}

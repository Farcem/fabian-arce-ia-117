import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Scanner;

public class Solution 
{
    public static class Edge 
    {
        private final Node startNode;
        private final Node destinyNode;
        private int weight;

        public Edge(Node node1, Node node2, int weight) 
        {
            this.startNode = node1;
            this.destinyNode = node2;
            this.weight = weight;
        }

        public Node startNode() {
            return startNode;
        }

        public Node destinyNode() {
            return destinyNode;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        @Override
        public String toString()
        {
            return weight + "(" + startNode.vertex + "," + destinyNode.vertex + ")";
        }

        public boolean isBetween(Node node1, Node node2) {
            return (this.startNode == node1 && this.destinyNode == node2);
        }
    }
    
    public static class Graph 
    {
        private final Map<Integer, Node> adjacencyList;

        public Graph(int initialNode) 
        {
            adjacencyList = new HashMap<>();
        }

        public boolean addNode(int node) 
        {
            if (adjacencyList.containsKey(node)) 
            {
                return false;
            }
            adjacencyList.put(node, new Node(node));
            return true;
        }

        public boolean addEdge(int fromNode, int toNode, int weight) 
        {
            if (!containsNode(fromNode) || !containsNode(toNode)) 
            {
                throw new RuntimeException("Node does not exist");
            }

            Node node1 = getNode(fromNode);
            Node node2 = getNode(toNode);
            return node1.addEdge(node2, weight) && node2.addEdge(node1, weight);
        }

        public boolean removeNode(int pNode) 
        {
            if (!adjacencyList.containsKey(pNode)) 
            {
                return false;
            }

            final Node toRemove = getNode(pNode);

            adjacencyList.values().forEach(node -> node.removeEdge(toRemove));

            adjacencyList.remove(pNode);
            return true;
        }

        public boolean removeEdge(int node1, int node2) 
        {
            if (!containsNode(node1) || !containsNode(node2)) 
            {
                return false;
            }
            return getNode(node1).removeEdge(getNode(node2)) && getNode(node2).removeEdge(getNode(node1));
        }

        public int nodeCount() 
        {
            return adjacencyList.keySet().size();
        }

        public int edgeCount() 
        {
            return adjacencyList.values()
                    .stream()
                    .mapToInt(Node::getEdgeCount)
                    .sum();
        }

        public boolean containsNode(int node) 
        {
            return adjacencyList.containsKey(node);
        }

        public boolean containsEdge(int node1, int node2) 
        {
            if (!containsNode(node1) || !containsNode(node2)) 
            {
                return false;
            }
            return getNode(node1).hasEdge(getNode(node2)) && getNode(node2).hasEdge(getNode(node1));
        }

        private Node getNode(int value) 
        {
            return adjacencyList.get(value);
        }

        private void resetGraph() 
        {
            adjacencyList.keySet().forEach(key -> {
                Node node = getNode(key);
                node.setParent(null);
                node.setVisited(false);
            });
        }

        public void print()
        {
            System.out.println("The Graph:- ");
            for(Map.Entry<Integer, Node> entry : adjacencyList.entrySet()){
                System.out.print(entry.getKey() + " -> ");
                for(Edge edge : entry.getValue().edges)
                    System.out.print(edge.toString() + " ");
                System.out.println("\n");
            }
        }
        
        public int breathFirstSearch(int startNode, int destinyNode)
        {
            resetGraph();

            Queue<Node> queue = new LinkedList<>();
            Queue<Node> queueTemp = new LinkedList<>();

            Node start = getNode(startNode);
            queue.add(start);
            int graphLevel = 1;

            while(!queue.isEmpty())
            {
                Node node = queue.poll();
                
                node.setVisited(true);
                for(Edge edge : node.edges())
                {                
                    Node child = edge.destinyNode();
                    if(!child.isVisited())
                    {
                        if(child.vertex() == destinyNode)
                        {
                            return graphLevel * edge.getWeight();
                        }
                        queueTemp.add(child);
                    }
                }

                if(queue.isEmpty())
                {
                    graphLevel++;
                    queue.addAll(queueTemp);
                    queueTemp.clear();
                }
            }
            return -1;
        }
    }
    
    public static class Node 
    {
        private int vertex;
        private List<Edge> edges;
        private Node parent;
        private boolean isVisited;

        public Node(int vertex) 
        {
            this.vertex = vertex;
            this.edges = new ArrayList<>();
        }

        public int vertex() 
        {
            return vertex;
        }

        public boolean addEdge(Node node, int weight) 
        {
            if (hasEdge(node)) 
                return false;

            Edge newEdge = new Edge(this, node, weight);
            return edges.add(newEdge);
        }

        public boolean removeEdge(Node node) 
        {
            Optional <Edge> optional = findEdge(node);
            if (optional.isPresent())
                return edges.remove(optional.get());

            return false;
        }

        public boolean hasEdge(Node node) 
        {
            return findEdge(node).isPresent();
        }

        private Optional<Edge> findEdge(Node node) 
        {
            return edges.stream()
                    .filter(edge -> edge.isBetween(this, node))
                    .findFirst();
        }

        public List<Edge> edges() 
        {
            return edges;
        }

        public int getEdgeCount() 
        {
            return edges.size();
        }

        public Node parent() 
        {
            return parent;
        }

        public boolean isVisited() 
        {
            return isVisited;
        }

        public void setVisited(boolean isVisited)
        {
            this.isVisited = isVisited;
        }

        public void setParent(Node parent) 
        {
            this.parent = parent;
        }
    }
    
    public static Graph createGraph(int pInitialNode, int pNodesNumber, int pEdgesNumber, List<Point> pNodeConnections)
    {
        Graph graph = new Graph(pInitialNode);
        
        for(int i = 1; i <= pNodesNumber; i++)
        {
            graph.addNode(i);
        }
        Point point;
        
        for(int i = 0; i < pEdgesNumber; i++)
        {
            point = pNodeConnections.get(i);
            graph.addEdge(point.x, point.y, 6);
        }
        return graph;
    }
    
    public static void main(String[] args) 
    {
        Scanner scanner = new Scanner(System.in);
        
        int graphNumbers = Integer.parseInt(scanner.nextLine());
        
        while(graphNumbers > 0)
        {
            String[] graphInformation = scanner.nextLine().split(" ");
            int nodesNumber = Integer.parseInt(graphInformation[0]);
            int edgesNumber = Integer.parseInt(graphInformation[1]);

            List<Point> nodeConnections = new ArrayList<>();

            int nodeA;
            int nodeB;

            for(int i = 0; i < edgesNumber; i++)
            {
                graphInformation = scanner.nextLine().split(" ");
                nodeA = Integer.parseInt(graphInformation[0]);
                nodeB = Integer.parseInt(graphInformation[1]);
            
                nodeConnections.add(new Point(nodeA, nodeB));
            }
            
            int initialNode = Integer.parseInt(scanner.nextLine().replaceAll(" ", ""));

            Graph graph = createGraph(initialNode, nodesNumber, edgesNumber, nodeConnections);
            //graph.print();
            
            for(int i = 1; i <= nodesNumber; i++)
            {
                if(i != initialNode)
                    System.out.print(graph.breathFirstSearch(initialNode,i) + " ");
            }
            
            System.out.println("");
            graphNumbers--;
        }
    }
}

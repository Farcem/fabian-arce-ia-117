
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

public class Solution 
{
    static int[][] puzzleSolved;
    final static HashMap<Integer, Point> solutionDataPositionHash = new HashMap<Integer, Point>();
    static int puzzleLength;
    
    public static class Node
    {
        private final int[][] grid;
        private Point spacePosition;
        private Node parent;
        private String move; 
        private int heuristicScore;
        private int cost;

        public int[][] getGrid() {return grid;}
        public Point getSpacePosition() {return spacePosition;}
        public void setSpacePosition(Point spacePosition) {this.spacePosition = spacePosition;}
        public Node getParent() {return parent;}
        public void setParent(Node parent) {this.parent = parent;}
        public String getMove() {return move;}
        public void setMove(String move) {this.move = move;}
        public int getHeuristicScore() {return heuristicScore + this.cost;}
        public int getCost(){return this.cost;};
        
        public Node(int[][] grid, Point spacePosition, String move) 
        {
            this.grid = grid;
            this.spacePosition = spacePosition;
            this.heuristicScore = this.setHeuristicScore();
            this.move = move;
            this.cost = 0;
        }
        
        public int setHeuristicScore()
        {
            int heuristicScoreValue = 0;
            int actualInt; 
            Point dataPositon;
            
            for(int row = 0; row < puzzleLength; row++)
            {
                for(int column = 0; column < puzzleLength; column++)
                {
                    actualInt = this.grid[row][column];
                    dataPositon = solutionDataPositionHash.get(actualInt);
                    heuristicScoreValue += Math.abs(dataPositon.x - row) + Math.abs(dataPositon.y - column);
                }
            }
            return heuristicScoreValue;
        }
        
        public void print() 
        {
            System.out.println("H: " + heuristicScore);
            for (int i = 0; i < puzzleLength; i++) 
            {
                for (int j = 0; j < puzzleLength; j++) 
                    System.out.print(grid[i][j]);
                System.out.println();
            }
            System.out.println("----------------------");
        }
        
        public void print(int[][] pGrid) 
        {
            for (int i = 0; i < puzzleLength; i++) 
            {
                for (int j = 0; j < puzzleLength; j++) 
                    System.out.print(pGrid[i][j]);
                System.out.println();
            }
        }
        
        private int[][] cloneGrid()
        {
            int[][] newGrid = new int[puzzleLength][puzzleLength];
            for (int i = 0; i < puzzleLength; i++) 
            {
                for (int j = 0; j < puzzleLength; j++)
                    newGrid[i][j] = this.grid[i][j];
            }
            return newGrid;
        }
        
        public void addCost(int cost)
        {
            this.cost += cost + this.parent.cost;
        }
    }
    
    private static void setSolutionData()
    {
        puzzleSolved = new int[puzzleLength][puzzleLength];
        
        int solutionData = 0;
        for(int row = 0; row < puzzleLength; row++)
            for(int column = 0; column < puzzleLength; column++)
            {
                puzzleSolved[row][column] = solutionData++;
                solutionDataPositionHash.put(puzzleSolved[row][column], new Point(row, column));
            }
    }
    
    private static ArrayList<Node> getNextPossibleMove(Node actualNode)
    {
        ArrayList<Node> possibleMoves = new ArrayList<>();
        int x = actualNode.getSpacePosition().x;
        int y = actualNode.getSpacePosition().y;
        
        //Check up
        if(x-1 >= 0)
        {
            int[][] gridMovedUp = actualNode.cloneGrid();
            gridMovedUp[x][y] = gridMovedUp[x-1][y];
            gridMovedUp[x-1][y] = 0;
            possibleMoves.add(new Node(gridMovedUp, new Point(x-1, y), "UP"));
        }
        //Check left
        if(y-1 >= 0)
        {
            int[][] gridMovedLeft = actualNode.cloneGrid();
            gridMovedLeft[x][y] = gridMovedLeft[x][y-1];
            gridMovedLeft[x][y-1] = 0;
            possibleMoves.add(new Node(gridMovedLeft, new Point(x, y-1), "LEFT"));

        }
        //Check right
        if(y+1 < puzzleLength)
        {
            int[][] gridMovedRight = actualNode.cloneGrid();
            gridMovedRight[x][y] = gridMovedRight[x][y+1];
            gridMovedRight[x][y+1] = 0;
            possibleMoves.add(new Node(gridMovedRight, new Point(x, y+1), "RIGHT"));

        }

        //Check down
        if(x+1 < puzzleLength)
        {
            int[][] gridMovedDown = actualNode.cloneGrid();
            gridMovedDown[x][y] = gridMovedDown[x+1][y];
            gridMovedDown[x+1][y] = 0;
            possibleMoves.add(new Node(gridMovedDown, new Point(x+1, y), "DOWN"));
        }
        return possibleMoves;
    }
    
    private static Node getMinPuzzleHeuristicScore(ArrayList<Node> moves)
    {
        int minHeuristicScore = Integer.MAX_VALUE;
        Node node = null;
        for(Node move : moves)
        {
            int score = move.getHeuristicScore();
            if(score < minHeuristicScore)
            {
                minHeuristicScore = score;
                node = move;
            }
        }
        return node;
    }
    
    private static boolean compareMatrix(int[][] grid1, int[][] grid2)
    {
        for(int row = 0; row < puzzleLength; row++)
            for(int column = 0; column < puzzleLength; column++)
                if(grid1[row][column] != grid2[row][column])
                    return false;
 
        return true;
    }
    
    private static void solvePuzzle(Node initialNode) throws IOException 
    {
        ArrayList<Node> openList = new ArrayList<>();
        ArrayList<Node> possibleNextMoves = new ArrayList<>();
        ArrayList<int[][]> closedList = new ArrayList<>();
        
        openList.add(initialNode);
        Node actualNode;
        Node solution = null;
        while(!openList.isEmpty())
        {
            actualNode = getMinPuzzleHeuristicScore(openList);
            //System.out.println(actualNode.getMove());
            
            //actualNode.print();
            
            //System.in.read();
            
            openList.remove(actualNode);
            closedList.add(actualNode.getGrid());
            
            if(compareMatrix(actualNode.getGrid(), puzzleSolved))
            {
                solution = actualNode;
                break;
            }
            possibleNextMoves = getNextPossibleMove(actualNode);
            for(Node move : possibleNextMoves)
            {   
                if(!isGridInList(move.getGrid(), closedList) && !openList.contains(move))
                {
                    move.setParent(actualNode);
                    move.addCost(1); 
                    openList.add(move);
                }
            }
        }
        
        Stack<String> path = new Stack<>();
        
        while(!compareMatrix(solution.getGrid(), initialNode.getGrid()))
        {
            path.add(solution.getMove());
            solution = solution.getParent();
        }
        
        System.out.println(path.size());
        while(!path.isEmpty())
        {
            System.out.println(path.peek());
            path.pop();
        }
    }
    
    private static boolean isGridInList(int[][] pGrid, ArrayList<int[][]> list)
    {
        for(int[][] grid : list)
            if(compareMatrix(grid, pGrid))
                return true;

        return false;
    }                    
    
    public static void main(String[] args) throws IOException 
    {
        Scanner scanner = new Scanner(System.in);
        puzzleLength = scanner.nextInt();
        int [][] initialPuzzle = new int[puzzleLength][puzzleLength];
        Point spacePosition = null;
        
        setSolutionData();
        
        for(int row = 0; row < puzzleLength; row++)
        {
            for(int column = 0; column < puzzleLength; column++)
            {
                initialPuzzle[row][column] = scanner.nextInt();
                if(initialPuzzle[row][column] == 0)
                    spacePosition = new Point(row, column);
            }
        }
        Node initialNode = new Node(initialPuzzle, spacePosition, "");
        
        solvePuzzle(initialNode);
    }
}

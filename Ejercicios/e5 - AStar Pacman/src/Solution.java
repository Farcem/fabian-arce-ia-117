import java.awt.Point;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Solution 
{
    private static Point food;
    private static Point pacman;

    public static class Node 
    {
        private final char character;
        private final int heuristic; //Es un numero fijo
        private int cost; //Es la distancia acumulada desde el nodo inicial a este, se inicia en cero y conforme se expande se va acumulando
        private boolean visited;
        private final Point location;
        private Node parent = null;
        
        public Node(char character, int x, int y, Point location) 
        {
            this.character = character;
            this.heuristic = Math.abs(food.x - x) + Math.abs(food.y - y);
            this.cost = 0;
            this.visited = false;
            this.location = location;
        }

        public Point getLocation() {
            return location;
        }
        
        public void setParent(Node parent) {
            this.parent = parent;
        }

        public Node getParent() {
            return parent;
        }
        
        public boolean isVisited() {
            return visited;
        }

        public void setVisited(boolean visited) {
            this.visited = visited;
        }
        
        public char getCharacter() {
            return character;
        }

        public int getHeuristic() {
            return heuristic;
        }

        public int getCost() {
            return cost;
        }

        public void setCost(int cost) {
            this.cost = cost;
        }    

        public void addCost(int cost) {
            this.cost += cost;
        }
        
        public int getFScore()
        {
            return this.cost + this.heuristic;
        }
    }
    
    public static class Matrix
    {
        private final int X;
        private final int Y;
        private final Node[][] data;

        public Matrix(Node[][] data)
        {
            X = data.length;
            Y = data[0].length;
            this.data = data;
        }

        public int getX() {
            return X;
        }

        public int getY() {
            return Y;
        }

        public void print() 
        {
            for (int i = 0; i < X; i++) 
            {
                for (int j = 0; j < Y; j++) 
                    System.out.print(data[i][j].getCharacter() + " - " + data[i][j].isVisited()+ "|");
                System.out.println();
            }
        }

        public Node getNode(int x, int y)
        {
            return this.data[x][y];
        }
    }

    public static Queue<Point> getUnvisitedNeighbours(Matrix grid, int x, int y)
    {
        Queue<Point> points = new LinkedList<>();
        
        //Check up
        if(x-1 >= 0 && !grid.getNode(x-1, y).isVisited() && grid.getNode(x-1, y).getCharacter() != '%')
            points.add(new Point(x-1, y));

        //Check left
        if(y-1 >= 0 && !grid.getNode(x, y-1).isVisited() && grid.getNode(x, y-1).getCharacter() != '%')
            points.add(new Point(x, y-1));

        //Check right
        if(y+1 < grid.getY() && !grid.getNode(x, y+1).isVisited() && grid.getNode(x, y+1).getCharacter() != '%')
            points.add(new Point(x, y+1));

        //Check down
        if(x+1 < grid.getX() && !grid.getNode(x+1, y).isVisited() && grid.getNode(x+1, y).getCharacter() != '%')
            points.add(new Point(x+1, y));

        return points;
    }
    
    private static Point getsNodeWithLowestFScore(Matrix grid, Queue<Point> openList)
    {
        Point lowestScorePoint = null;
        int FScore = Integer.MAX_VALUE;
        for(Point point : openList)
        {
            if(grid.getNode(point.x, point.y).getFScore() < FScore)
            {
                FScore = grid.getNode(point.x, point.y).getFScore();
                lowestScorePoint = point;
            }
        }
        return lowestScorePoint;
    }
    
    private static void aStarSearch(Matrix grid) 
    {
        Queue<Point> openList = new LinkedList<>();
        Queue<Point> unvisitedNeighbours;
        Point actualNode;

        openList.add(pacman);
        
        while(!openList.isEmpty())
        {
            actualNode = getsNodeWithLowestFScore(grid, openList);
            
            openList.remove(actualNode);

            if(actualNode.equals(food))// Encontro la comida, termina el ciclo
                break;
            
            unvisitedNeighbours = getUnvisitedNeighbours(grid, actualNode.x, actualNode.y);
            
            for(Point point : unvisitedNeighbours) 
            {
                grid.getNode(point.x, point.y).setVisited(true);
                grid.getNode(point.x, point.y).setParent(grid.getNode(actualNode.x, actualNode.y));
                
                if(!point.equals(food))
                    grid.getNode(point.x, point.y).addCost(1);
                
                if(!openList.contains(point))
                    openList.add(point);
            }
        }
        
        Stack<Point> directPath = new Stack<>();
        Node tarjet = grid.getNode(food.x, food.y);
        
        while(!tarjet.getLocation().equals(pacman))
        {
            directPath.add(tarjet.getLocation());
            tarjet = tarjet.getParent();
        }
        directPath.add(pacman);
        
        System.out.println(directPath.size()-1);
        while(!directPath.isEmpty())
        {
            System.out.println(directPath.peek().x + " " + directPath.peek().y);
            directPath.pop();
        }
    }
  
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        int pacman_x = in.nextInt(); //The first line contains 2 space separated integers which is the position of the PacMan. 
        int pacman_y = in.nextInt(); 

        int food_x = in.nextInt(); //The second line contains 2 space separated integers which is the position of the food. 
        int food_y = in.nextInt();

        pacman = new Point(pacman_x, pacman_y);
        food = new Point(food_x, food_y);
        
        int grid_x = in.nextInt(); //The third line of the input contains 2 space separated integers indicating the size of the rows and columns 
        int grid_y = in.nextInt();
    
        Node[][] grid = new Node[grid_x][grid_y];
        
        String row;
        for(int x = 0; x < grid_x; x++) 
        {
            row = in.next();

            for(int y = 0; y < grid_y; y++)
                grid[x][y] = new Node(row.charAt(y), x, y, new Point(x,y));
        }
        
        Matrix gridNode = new Matrix(grid);
        aStarSearch(gridNode);
    }
}

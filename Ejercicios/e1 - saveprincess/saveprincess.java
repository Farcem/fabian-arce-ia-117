import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    static int xHero=0;
    static int yHero=0;
    static int xPrincess=0;
    static int yPrincess=0;
static void displayPathtoPrincess(int n, String [] grid){
    getHeroPos(n, grid);
    getPrincessPos(n, grid);
    
    while (xHero!=xPrincess && yHero!=yPrincess){
	if (xPrincess < xHero){
	    xHero--;
	    System.out.println("LEFT");
	}
	if (xPrincess > xHero){
	    xHero++;
	    System.out.println("RIGHT");
	}
	if (yPrincess < yHero){
	    yHero--;
	    System.out.println("UP");
	}
	if (yPrincess > yHero){
	    yHero++;
	    System.out.println("DOWN");
	}
    }
    
  }

static void getHeroPos(int n, String[] grid){
    for (int i = 0; i < n; i++){
        if (grid[i].contains("m")){
            xHero = grid[i].indexOf("m");
            yHero = i;   
        }
  }
}

static void getPrincessPos(int n, String[] grid){
    for (int i = 0; i < n; i++){
        if (grid[i].contains("p")){
            xPrincess = grid[i].indexOf("p");
            yPrincess = i;   
        }
  }
}
public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m;
        m = in.nextInt();
        String grid[] = new String[m];
        for(int i = 0; i < m; i++) {
            grid[i] = in.next();
        }
    displayPathtoPrincess(m,grid);
    }
}

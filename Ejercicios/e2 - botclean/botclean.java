import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Solution
{
    static void next_move(int initialX, int initialY, String[] board)
    {
        Point currentBotPosition = new Point(initialX, initialY);
        List<Point> dirtyCellLocations = getDirtyCellLocations(board);

        if (currentBotPosition.x == dirtyCellLocations.get(0).x && currentBotPosition.y == dirtyCellLocations.get(0).y)
        {
            System.out.println("CLEAN");
            dirtyCellLocations.remove(0);
        }
        else
            updateBotPosition(currentBotPosition, dirtyCellLocations.get(0));

    }

    static List<Point> getDirtyCellLocations(String[] board)
    {
        List<Point> cellLocations = new ArrayList<>();

        for (int x = 0; x < board.length; x++)
            for (int y = 0; y < board[x].length(); y++)
                if (board[x].charAt(y) == 'd')
                    cellLocations.add(new Point(x, y));

        return cellLocations;
    }

    static Point updateBotPosition(Point pBotPosition, Point pDirtyCellPosition)
    {
        if (pBotPosition.x < pDirtyCellPosition.x)
        {
            pBotPosition.x++;
            System.out.println("DOWN");
        }
        else if (pBotPosition.x > pDirtyCellPosition.x)
        {
            pBotPosition.x--;
            System.out.println("UP");
        }
        else if (pBotPosition.y < pDirtyCellPosition.y)
        {
            pBotPosition.y++;
            System.out.println("RIGHT");
        }
        else
        {
            pBotPosition.y--;
            System.out.println("LEFT");
        }

        return pBotPosition;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int [] pos = new int[2];
        String board[] = new String[5];
        for(int i=0;i<2;i++) pos[i] = in.nextInt();
        for(int i=0;i<5;i++) board[i] = in.next();
        next_move(pos[0], pos[1], board);
    }
}
